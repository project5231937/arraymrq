public class Operation {
    public Operation() {
    }
    public int[] process(int[] input) {
        boolean isSorted = false;

        do {
            isSorted = true;

            for(int i = 0; i < input.length - 1; ++i) {
                if (input[i] > input[i + 1]) {
                    int temp = input[i];
                    input[i] = input[i + 1];
                    input[i + 1] = temp;
                    isSorted = false;
                }
            }
        } while(!isSorted);

        return input;
    }
}
//Yes